import React from 'react';
import './main.scss';
import Grid from '../Components/Grid/Grid';
import SectionMainServices from '../Content/Sections/Services_main';
import SectionGallery from '../Content/Sections/Gallery';
import SectionNumbers from '../Content/Sections/Numbers';
import SectionSliderHome from '../Content/Sections/SliderHome';

const Page = props => {
    return (
        <div className="main">

            <SectionSliderHome />

            <SectionMainServices />

            <SectionGallery />

            <SectionNumbers />

        </div>
    );
};


export default Page;
