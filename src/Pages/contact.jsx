import React from 'react';
import './main.scss';
import Grid from '../Components/Grid/Grid';
import ContactForm from '../Content/Form/Contact/Contact';
import Widget from '../Content/Widget/Widget';
import Slider from '../Content/Slider/Slider';
import Section from '../Components/Section/Section';
import Container from '../Components/Container/Container';
import SectionSliderHome from '../Content/Sections/SliderHome';

const Page = props => {
    return (
        <div class="main">

            <SectionSliderHome />

            <Section mod="contact" className="main__primary">
                <Container>
                    <Grid mod="main--sidebar">
                        <>
                            <ContactForm />
                        </>
                        <>
                            <Widget />
                        </>
                    </Grid>
                </Container>
            </Section>
        </div>
    );
};


export default Page;
