import React from 'react';
import './main.scss';
import Grid from '../Components/Grid/Grid';
import SectionMainServices from '../Content/Sections/Services_main';
import SectionSliderHome from '../Content/Sections/SliderHome';

const Page = props => {
    return (
        <div className="main--offers">

            <SectionSliderHome />

            <SectionMainServices />

            <SectionMainServices />

            <SectionMainServices />

            <SectionMainServices />

        </div>
    );
};


export default Page;
