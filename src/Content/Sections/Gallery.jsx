import React from 'react';
import Container from '../../Components/Container/Container';
import Section from '../../Components/Section/Section';
import Text from '../../Components/Text/Text';
import Gallery from '../../Components/Gallery/Gallery';

const getImage = () => {
    return "http://www.ippf.org/static/services_ipes.png";
};

const getElements = () => {
    const items = [
        getImage(),
        getImage(),
        getImage(),
        getImage(),
        getImage(),
        getImage(),
        getImage(),
        getImage(),
    ];
    return items.map((item,key)=>{
        return (<img key={key} src={item} alt="d" className="gallery__img" />);
    });
};

const Content = props => {
    const items = getElements();
    return (
        <Section>
            <Container>
                <Text type="title" mod="title--section" >Galeria</Text>
                <div>
                    <Gallery>
                        {items}
                    </Gallery>
                </div>
            </Container>
        </Section>
    );
};

export default Content;
