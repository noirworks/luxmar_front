import React from 'react';
import Container from '../../Components/Container/Container';
import Section from '../../Components/Section/Section';
import Text from '../../Components/Text/Text';
import Numbers from '../../Components/Numbers/Numbers';

const getElements = () => {
    const items = [
        {
            label: "Lat doświadczenia",
            num: 24
        },
        {
            label: "Zadowolonych klientów",
            num: 100
        },
        {
            label: "Wyczyszczonych metrów",
            num: 100000
        }
    ];
    return items;
};

const Content = props => {
    const items = getElements();
    return (
        <Section>
            <Container>
                <div>
                    <Numbers items={items} />
                </div>
            </Container>
        </Section>
    );
};

export default Content;
