import React from 'react';
import Container from '../../Components/Container/Container';
import Section from '../../Components/Section/Section';
import Text from '../../Components/Text/Text';

import Slider from '../../Components/SlickSlider/Slider';
import SliderItem from '../../Components/SlickSlider/Item';
import Card from '../../Components/Card/Slide';

const getImage = () => {
    return (<img src="http://www.ippf.org/static/services_ipes.png" alt="d" />);
};

const getElements = () => {
    const items = [
        getImage(),
        getImage(),
        getImage(),
        getImage(),
        getImage(),
    ];
    return items.map((item,key)=>{
        return (<div>{item}</div>);
    });
};

const Content = props => {
    const items = getElements();
    var settings = {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        //autoplay: true,
        arrows: false
    };
    return (
        <Section>
            <Container>
                <div >
                    <Slider settings={settings} >
                        <>
                            <Card title="Inny tytuł" imgSrc="https://sample-videos.com/img/Sample-jpg-image-500kb.jpg" />
                        </>
                        <>
                            <Card title="Inny tytuł" />
                        </>
                        <>
                            <Card title="Inny tytuł" />
                        </>
                        <>
                            <Card title="Inny tytuł" />
                        </>
                        <>
                            <Card title="Inny tytuł" />
                        </>
                        <>
                            <Card title="Inny tytuł" />
                        </>
                    </Slider>
                </div>
            </Container>
        </Section>
    );
};

export default Content;
