import React from 'react';
import Input from '../../../Components/Input/Input';
import Grid from '../../../Components/Grid/Grid';
import Row from '../../../Components/Row/Row';
import Text from '../../../Components/Text/Text';
import Btn from '../../../Components/Btn/Btn';

class Form extends React.Component{

    state = {
        firstname: '',
        surname: '',
        mail: '',
        phone: '',
        service: '',
        message: ''
    };

    handlers = {
        firstname: (value) => {
            this.setState({ firstname: value });
        },
        surname:  (value) => {
            this.setState({ surname: value });
        },
        mail:  (value) => {
            this.setState({ mail: value });
        },
        phone:  (value) => {
            this.setState({ phone: value });
        },
        service:  (value) => {
            this.setState({ service: value });
        },
        message:  (value) => {
            this.setState({ message: value });
        }
    };

    render = () => {
        return (
            <div>
                <Grid mod="contact-form">
                    <Row mod="contact-form">
                        <Text type="title">Skontaktuj się z nami</Text>
                    </Row>
                    <Row mod="contact-form">
                        <Grid mod="fields--2">
                            <Grid mod="field--half">
                                <Text type="label">Imię</Text>
                                <Input
                                    type="text"
                                    value={this.state.firstname}
                                    onChange={this.handlers.firstname}
                                    placeholder="Imię"
                                />
                            </Grid>
                            <Grid mod="field--half">
                                <Text type="label">Nazwisko</Text>
                                <Input
                                    type="text"
                                    value={this.state.surname}
                                    onChange={this.handlers.surname}
                                    placeholder="Nazwisko"
                                />
                            </Grid>
                        </Grid>
                    </Row>
                    <Row mod="contact-form">
                        <Grid mod="fields--2">
                            <Grid mod="field--half">
                                <Text type="label">Mail</Text>
                                <Input
                                    type="text"
                                    value={this.state.mail}
                                    onChange={this.handlers.mail}
                                    placeholder="Mail"
                                />
                            </Grid>
                            <Grid mod="field--half">
                                <Text type="label">Telefon</Text>
                                <Input
                                    type="text"
                                    value={this.state.phone}
                                    onChange={this.handlers.phone}
                                    placeholder="Telefon"
                                />
                            </Grid>
                        </Grid>
                    </Row>
                    <Row mod="contact-form">
                        <Grid mod="fields--1">
                            <Grid mod="field">
                                <Text type="label">Usługa</Text>
                                <Input
                                    type="text"
                                    value={this.state.service}
                                    onChange={this.handlers.service}
                                    placeholder="Usługa"
                                />
                            </Grid>
                        </Grid>
                    </Row>
                    <Row mod="contact-form">
                        <Grid mod="fields--1">
                            <Grid mod="field">
                                <Text type="label">Wiadomość</Text>
                                <Input
                                    type="textarea"
                                    value={this.state.message}
                                    onChange={this.handlers.message}
                                    placeholder="Wiadomość"
                                />
                            </Grid>
                        </Grid>
                    </Row>
                    <Row mod="contact-form">
                        <Btn mod="primary" size="md" >Wyślij</Btn>
                    </Row>
                </Grid>
            </div>
        );
    };

}

export default Form;
