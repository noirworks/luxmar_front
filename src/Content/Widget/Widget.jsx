import React from 'react';
import Row from '../../Components/Row/Row';
import Grid from '../../Components/Grid/Grid';
import Text from '../../Components/Text/Text';

const Page = props => {
    return (
        <div>
            <Grid mod="widget">
                <>
                    <Row mod="widget-item">
                        <Grid mod="widget-item">
                            <Text>Adres</Text>
                            <Text>6036 Richmond hwy., Alexandria, VA, 2230</Text>
                        </Grid>
                    </Row>
                </>
                <>
                    <Row mod="widget-item">
                        <Grid mod="widget-item">
                            <Text>Telefon</Text>
                            <Text>601 211 093</Text>
                        </Grid>
                    </Row>
                </>
                <>
                    <Row mod="widget-item">
                        <Grid mod="widget-item">
                            <Text>E-mail</Text>
                            <Text>luxmar@luxmar.waw.pl</Text>
                        </Grid>
                    </Row>
                </>
            </Grid>
        </div>
    );
};


export default Page;
