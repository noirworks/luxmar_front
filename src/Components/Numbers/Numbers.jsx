import React from 'react';

const Numbers = props => {
    let items = props.items.map((item,key)=>{
        return (
            <div key={key}>
                <p>{item.num}</p>
                <p>{item.label}</p>
            </div>
        );
    });
    return (
        <div>
            <div>
                {items}
            </div>
        </div>
    );
};

export default Numbers;
