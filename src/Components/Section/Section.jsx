import React from 'react';
import './Section.scss';

const defaultProps = {
    className: '',
    mod: '',
    children: null
};

const getClassName = (props) => {
    let className = 'section';
    if(props.mod){ className += ' section--' + props.mod; };
    if(props.className){ className += ' ' + props.className; };
    return className;
};

const Section = props => {
    props = { ...defaultProps, ...props };
    const className = getClassName(props);
    return (
        <div className={className}>
            { props.children }
        </div>
    );
};

export default Section;
