import React from 'react';

const Item = props => {
    return (
        <div className="slider__item">
            { props.children }
        </div>
    );
};

export default Item;
