import React from 'react';
import Slider from 'react-slick';

//https://www.npmjs.com/package/react-slick-slider

class SimpleSlider extends React.Component {

    getChildren = (child,key) => {
        return (
            <div key={key} className="slider__item">
                {child}
            </div>
        );
    };

    getChildrens = (props) => {
        if(props.children){
            if(props.children.length){
                return props.children.map((child,key)=>{return this.getChildren(child,key);});
            }else{
                return this.getChildren(props.children,0);
            }
        }
        return null;
    };

    render = () => {
        let settings = {
            dots: true,
            infinite: true,
            speed: 500,
            slidesToShow: 1,
            slidesToScroll: 1
        };
        let childrens = this.getChildrens(this.props);
        settings = { ...settings, ...this.props.settings };
        return (
            <Slider {...settings} className="slider--slick" >
                { childrens }
            </Slider>
        );
    };

}

export default SimpleSlider;
