import React from 'react';
import './Menu.scss';
import Btn from '../Btn/Btn';

const getChildren = (child,key) => {
    return (
        <div key={key} className="menu__item">
            {child}
        </div>
    );
};

const getChildrens = (props) => {
    if(props.children){
        if(props.children.length){
            return props.children.map((child,key)=>{return getChildren(child,key);});
        }else{
            return getChildren(props.children,0);
        }
    }
    return null;
};

const Menu = (props) => {
    let childrens = getChildrens(props);
    return (
        <div className="menu">
            <ul className="menu__list">
                { childrens }
            </ul>
        </div>
    );
}

export default Menu;
