import React from 'react';
import './Row.scss';

const defaultProps = {
    className: '',
    mod: '',
    children: null
};

const getClassName = (props) => {
    let className = 'row';
    if(props.mod){ className += ' row--' + props.mod; };
    if(props.className){ className += ' ' + props.className; };
    return className;
};

const Row = props => {
    props = { ...defaultProps, ...props };
    const className = getClassName(props);
    return (
        <div className={className}>
            { props.children }
        </div>
    );
};

export default Row;
