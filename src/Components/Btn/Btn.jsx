import React from 'react';
import { Link } from "react-router-dom";
import './Btn.scss';

const defaultProps = {
    size: '',
    className: '',
    mod: 'primary',
    children: null,
    tag: 'a'
};

const getClassName = (props) => {
    let className = 'btn';
    if(props.mod){ className = 'btn--' + props.mod; };
    if(props.className){ className += ' ' + props.className; };
    if(props.size){ className += ' ' + 'btn--'+props.size; };
    return className;
};

const Btn = props => {
    props = { ...defaultProps, ...props };
    const className = getClassName(props);
    switch (props.tag){
        case 'a':
            return(
                <a {...props} className={className}>
                    {props.children}
                </a>
            );
        case 'button':
            return(
                <button {...props} className={className} >
                    {props.children}
                </button>
            );
        case 'link':
        case 'Link':
            return(
                <Link {...props} className={className} to={props.to} >
                    {props.children}
                </Link>
            );
    }
    return null;
};

export default Btn;
