import React from 'react';
import './Text.scss';

const defaultProps = {
    size: '',
    className: '',
    mod: '',
    children: null,
    type: 'text',
    h: 3
};

const getClassName = (props) => {
    let className = 'text';
    if(props.mod){ className = 'text--' + props.mod; };
    if(props.className){ className += ' ' + props.className; };
    if(props.size){ className += ' ' + 'text--'+props.size; };
    return className;
};

const Text = props => {
    props = { ...defaultProps, ...props };
    const className = getClassName(props);
    switch (props.type){
        case 'paragraph':
            return(
                <p {...props} className={className} >
                    {props.children}
                </p>
            );
        case 'title':
            return(
                <h3 {...props} className={className} >
                    {props.children}
                </h3>
            );
        case 'label':
            return(
                <label {...props} className={className} >
                    {props.children}
                </label>
            );
        case 'text':
        default:
            return(
                <span {...props} className={className} >
                    {props.children}
                </span>
            );
    }
    return null;
};

export default Text;
