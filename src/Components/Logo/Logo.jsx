import React from 'react';
import './Logo.scss';
import LogoSrc from './Logo.png';

const Logo = props => (
    <div className="logo">
        <img src={LogoSrc} alt="Logo Luxmar" />
    </div>
);

export default Logo;
