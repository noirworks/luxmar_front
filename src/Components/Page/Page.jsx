import React from 'react';
import './Page.scss';
import Header from './Page/Header';
import Body from './Page/Body';
import Footer from './Page/Footer';

const Page = (props) => (
    <div className="page">
        <Header>{props.children[0]}</Header>
        <Body></Body>
        <Footer>{props.children[2]}</Footer>
    </div>
);

export default Page;
