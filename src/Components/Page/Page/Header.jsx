import React from 'react';
import Row from '../../Row/Row';
import Grid from '../../Grid/Grid';
import Logo from '../../Logo/Logo';
import Menu from '../../Menu/Menu';
import Container from '../../Container/Container';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import Btn from '../../Btn/Btn';

const Header = props => {
    return (
        <div className="page__header">
            <Row>
                <Container>

                </Container>
            </Row>
            <Row>
                <Container>
                    <Grid mod="topbar">
                        <>
                            <Logo />
                        </>
                        <>
                            <Menu>
                                <Btn mod="menu" tag="link" to="/">Home</Btn>
                                <Btn mod="menu" tag="link" to="/offers">Oferta</Btn>
                                <Btn mod="menu" tag="link" to="/contact">Kontakt</Btn>
                            </Menu>
                        </>
                    </Grid>
                </Container>
            </Row>
        </div>
    );
};

export default Header;
