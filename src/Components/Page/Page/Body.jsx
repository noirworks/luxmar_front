import React from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import PageHome from '../../../Pages/home';
import PageContact from '../../../Pages/contact';
import PageOffers from '../../../Pages/offers';

const Body = props => {
    return (
        <div className="page__body">
            <Route path="/" exact component={ PageHome } />
            <Route path="/offers" exact component={ PageOffers } />
            <Route path="/contact" exact component={ PageContact } />
        </div>
    );
};

export default Body;
