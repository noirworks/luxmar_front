import React from 'react';
import Grid from '../../Grid/Grid';
import Container from '../../Container/Container';
import Section from '../../Section/Section';
import Row from '../../Row/Row';
import Text from '../../Text/Text';

const Footer = props => {
    return (
        <div className="page__footer">
            <Section mod="meta">
                <Container>
                    <Row>
                        <Grid mod="meta">
                            <>
                                <Text type="title">Luxmar</Text>
                                <Text type="paragraph" mod="footer-decription">Lorem ipsum dolor sit amet enim. Etiam ullamcorper. Suspendisse a pellentesque dui, non felis. Maecenas malesuada elit lectus felis, malesuada ultricies.</Text>
                            </>
                            <>
                                <Text type="title">Luxmar</Text>
                                <Row mod="meta">
                                    <Text type="title" >Adres</Text>
                                    <Text type="paragraph">Al. Prymasa Tysiąclecia 60/62 01-424 Warszawa</Text>
                                </Row>
                                <Row mod="meta">
                                    <Text type="title" >Telefon</Text>
                                    <Text type="paragraph">601 211 093</Text>
                                </Row>
                                <Row mod="meta">
                                    <Text type="title" >Email</Text>
                                    <Text type="paragraph">strolux@strolux.waw.pl</Text>
                                </Row>
                            </>
                        </Grid>
                    </Row>
                </Container>
            </Section>
            <Section mod="copy">
                <Container>
                    <Row>
                        <Text>© 2019. All Rights Reserved. Privacy policy</Text>
                    </Row>
                </Container>
            </Section>
        </div>
    );
};

export default Footer;
