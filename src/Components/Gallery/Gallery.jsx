import React from 'react';
import './Gallery.scss';

class Gallery extends React.Component{

    getChildren = (child,key) => {
        return (
            <div key={key} className="gallery__item" data-gallery-item={key} >
                {child}
            </div>
        );
    };

    getChildrens = (props) => {
        if(props.children){
            if(props.children.length){
                return props.children.map((child,key)=>{return this.getChildren(child,key);});
            }else{
                return this.getChildren(props.children,0);
            }
        }
        return null;
    };

    render = () => {
        let childrens = this.getChildrens(this.props);
        return (
            <div className="gallery">
                <div className="gallery__grid">
                    {childrens}
                </div>
            </div>
        );
    };

}

export default Gallery;
