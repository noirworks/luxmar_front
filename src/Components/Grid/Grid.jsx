import React from 'react';
import './Grid.scss';

const defaultProps = {
    className: '',
    mod: '',
    children: null
};

const getChildren = (child,key) => {
    return (
        <div key={key} className="grid__item">
            {child}
        </div>
    );
};

const getChildrens = (props) => {
    if(props.children){
        if(props.children.length){
            return props.children.map((child,key)=>{return getChildren(child,key);});
        }else{
            return getChildren(props.children,0);
        }
    }
    return null;
};

const getClassName = (props) => {
    let className = 'grid';
    if(props.mod){ className = ' grid--' + props.mod; };
    if(props.className){ className += ' ' + props.className; };
    return className;
};

const Grid = props => {
    props = { ...defaultProps, ...props };
    let childrens = getChildrens(props);
    const className = getClassName(props);
    return (
        <div className={className}>
            { childrens }
        </div>
    );
};

export default Grid;
