import React from 'react';
import './Task.scss';
import Btn from '../Btn/Btn';
import txt from '../../Content/lang';

const Card = props => {
    const task = props.task;
    let deadline = null;
    if(props.task.deadline && props.task.deadline.date){
        let date = new Date(props.task.deadline.date);
        deadline = date.getFullYear() + '-' + date.getMonth() + '-' + date.getDate() + ' ' + date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds();
    }
    return (
        <Btn
            className="task--card"
            tag="link"
            mod="wrap"
            to={"/task/"+task.id}
            onlyWrap={true}
        >
            <div className="task__row">
                <span className="task__label">
                    {txt.subject}:
                </span>
                <span className="task__value">
                    {task.subject}
                </span>
            </div>
            <div className="task__row">
                <span className="task__label">
                    {txt.category}:
                </span>
                <span className="task__value">
                    {task.category}
                </span>
            </div>
            <div className="task__row">
                <span className="task__label">
                    {txt.description}:
                </span>
                <span className="task__value">
                    {task.description}
                </span>
            </div>
            <div className="task__row">
                <span className="task__label">
                    {txt.priority}:
                </span>
                <span className="task__value">
                    {task.priority}
                </span>
            </div>
            <div className="task__row">
                <span className="task__label">
                    {txt.deadline}:
                </span>
                <span className="task__value">
                    {deadline}
                </span>
            </div>
        </Btn>
    );
};

export default Card;
