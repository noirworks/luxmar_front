import React from 'react';

const defaultProps = {
    type: "text",
    value: "",
    onChange: null,
    options: []
};

const Input = props => {
    props = { ...defaultProps, ...props };
    let options = props.options.map((opt, key)=>{
        return (<option key={key} value={opt.value}>{opt.label || opt.value}</option>);
    });
    return (
        <select
            className="input--select"
            value={props.value}
            onChange={(el)=>props.onChange(el.target.value)}
        >
            { options }
        </select>
    );
};

export default Input;
