import React from 'react';
import DateTimePicker from 'react-datetime-picker';

const defaultProps = {
    type: "text",
    value: "",
    onChange: null,
    options: []
};

const Input = props => {
    props = { ...defaultProps, ...props };
    return (
        <DateTimePicker
            className="input--datetime"
            onChange={props.onChange}
            value={props.value}
            disableClock={true}
        />
    );
};

export default Input;
