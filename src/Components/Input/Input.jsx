import React from 'react';
import './Input.scss';
import Datetime from './Input/Datetime';
import Search from './Input/Search';
import Select from './Input/Select';
import Text from './Input/Text';
import Textarea from './Input/Textarea';

const defaultProps = {
    type: "text",
    value: "",
    onChange: (val)=>{ return null; },
    options: [],
    placeholder: null
};

const Section = props => {
    props = { ...defaultProps, ...props };
    switch(props.type){
        case 'datetime':  return ( <Datetime {...props} /> );
        case 'search':  return ( <Search {...props} /> );
        case 'select': return ( <Select {...props} /> );
        case 'text':  return ( <Text {...props} /> );
        case 'textarea':  return ( <Textarea {...props} /> );
    };
    return null;
};

export default Section;
